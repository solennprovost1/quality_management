import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CustomersModule } from './modules/customers/customers.module';
import { CoreModule } from './core/core.module';
import { MaterialsModule } from './shared/materials/materials.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, BrowserAnimationsModule, HttpClientModule, AppRoutingModule, CustomersModule, CoreModule, MaterialsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
