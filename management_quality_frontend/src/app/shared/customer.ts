export interface Customer {
  id?: number;
  firstname: string;
  lastname: string;
  dateOfBirth: string;
  address: string;
  zipCode: string;
  city: string;
}
