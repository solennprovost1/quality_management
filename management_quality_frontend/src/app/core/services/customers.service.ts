import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Customer } from 'src/app/shared/customer';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};
@Injectable({
  providedIn: 'root',
})
export class CustomersService {
  constructor(private http: HttpClient) {}

  getCustomersList(): Observable<Customer[]> {
    return this.http.get<Customer[]>('http://localhost:8080/customer');
  }

  addCustomer(customer: Customer): Observable<Customer> {
    return this.http.post<Customer>('http://localhost:8080/customer', customer);
  }

  getCustomerById(id: number): Observable<Customer> {
    return this.http.get<Customer>('http://localhost:8080/customer' + '/' + id);
  }

  deleteCustomer(id: number): Observable<void> {
    return this.http.delete<void>('http://localhost:8080/customer' + '/' + id);
  }

  updateCustomer(customer: Customer): Observable<Customer> {
    return this.http.patch<Customer>('http://localhost:8080/customer', customer);
  }
}
