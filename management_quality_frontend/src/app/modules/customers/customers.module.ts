import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomersList } from './components/customersList/customersList.component';
import { CustomersRoutingModule } from './customers-routing.module';
import { CustomersAddComponent } from './components/customers-add/customers-add.component';
import { MaterialsModule } from 'src/app/shared/materials/materials.module';
import { FormsModule } from '@angular/forms';
import { CustomerDetailsComponent } from './components/customer-details/customer-details.component';
import { CustomerUpdateComponent } from './components/customer-update/customer-update.component';

@NgModule({
  declarations: [CustomersList, CustomersAddComponent, CustomerDetailsComponent, CustomerUpdateComponent],
  imports: [CommonModule, FormsModule, CustomersRoutingModule, MaterialsModule],
  exports: [CustomersList],
})
export class CustomersModule {}
