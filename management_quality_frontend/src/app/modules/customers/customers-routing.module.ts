import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CustomersList } from './components/customersList/customersList.component';
import { CustomersAddComponent } from './components/customers-add/customers-add.component';
import { CustomerDetailsComponent } from './components/customer-details/customer-details.component';
import { CustomerUpdateComponent } from './components/customer-update/customer-update.component';

const routes: Routes = [
  { path: '', component: CustomersList },
  { path: 'add', component: CustomersAddComponent },
  { path: 'details/:id', component: CustomerDetailsComponent },
  { path: 'edit/:id', component: CustomerUpdateComponent },
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomersRoutingModule {}
