import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomersService } from 'src/app/core/services/customers.service';
import { Customer } from 'src/app/shared/customer';

@Component({
  selector: 'app-customer-update',
  templateUrl: './customer-update.component.html',
  styleUrls: ['./customer-update.component.scss'],
})
export class CustomerUpdateComponent {
  customerId: number | null | undefined;
  customer: Customer = {
    firstname: '',
    lastname: '',
    dateOfBirth: '',
    address: '',
    zipCode: '',
    city: '',
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private customersService: CustomersService
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe((params) => {
      const id = params.get('id');
      if (id !== null) {
        this.customerId = +id;
        this.loadCustomerDetails();
      }
    });
  }

  loadCustomerDetails() {
    if (this.customerId) {
      this.customersService.getCustomerById(this.customerId).subscribe(
        (customer: Customer) => {
          this.customer = customer;
        },
        (error) => {
          console.error('Error retrieving customer details:', error);
        }
      );
    }
  }

  updateCustomer() {
    if (this.isValidForm()) {
      this.customersService.updateCustomer(this.customer).subscribe({
        next: (result) => {
          // Handle successful update
        },
        error: (error) => {
          console.error('Error updating customer:', error);
        },
        complete: () => {
          this.router.navigate(['/customers']);
        }
      });
    }
  }


  isValidForm() {
    // Ajoutez ici votre logique de validation du formulaire si nécessaire
    return true;
  }
}
