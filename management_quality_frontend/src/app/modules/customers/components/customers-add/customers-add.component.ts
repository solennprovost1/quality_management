import { Component, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CustomersService } from 'src/app/core/services/customers.service';
import { Customer } from 'src/app/shared/customer';

@Component({
  selector: 'app-customers-add',
  templateUrl: './customers-add.component.html',
  styleUrls: ['./customers-add.component.scss'],
})
export class CustomersAddComponent {
  constructor(
    private router: Router,
    private customersService: CustomersService
  ) {}

  @Output() customerAdded = new EventEmitter<Customer>();
  customer: Customer = {
    firstname: '',
    lastname: '',
    dateOfBirth: '',
    address: '',
    zipCode: '',
    city: '',
  };

  async addCustomer() {
    if (this.isValidForm()) {
      try {
        await this.customersService.addCustomer(this.customer).toPromise();

        this.customer = {
          firstname: '',
          lastname: '',
          dateOfBirth: '',
          address: '',
          zipCode: '',
          city: '',
        };

        this.router.navigate(['/customers']);
      } catch (error) {
        console.error('Error adding customer:', error);
      }
    }
  }

  isValidForm() {
    return true;
  }
}
