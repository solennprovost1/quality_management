import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CustomersService } from 'src/app/core/services/customers.service';
import { Customer } from 'src/app/shared/customer';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.scss'],
})
export class CustomerDetailsComponent {
  public customerId: number = 0;
  customer: Customer | null = null;

  constructor(
    private route: ActivatedRoute,
    private customersService: CustomersService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.customerId = +params['id']; // Convertir le paramètre en nombre
      this.getCustomerDetails();
    });
  }

  getCustomerDetails(): void {
    this.customersService.getCustomerById(this.customerId).subscribe({
      next: (customer: Customer) => {
        this.customer = customer;
      },
      error: (error) => {
        console.error('Error retrieving customer details:', error);
      }
    });
  }

}
