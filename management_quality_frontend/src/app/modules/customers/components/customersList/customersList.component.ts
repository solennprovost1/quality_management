import { Component, ViewChild } from '@angular/core';
// import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { CustomersService } from 'src/app/core/services/customers.service';
import { Customer } from 'src/app/shared/customer';

@Component({
  selector: 'app-customersList',
  templateUrl: './customersList.component.html',
  styleUrls: ['./customersList.component.scss'],
})
export class CustomersList {
  displayedColumns = [
    'firstname',
    'lastname',
    'dateOfBirth',
    'address',
    'zipCode',
    'city',
    'action',
  ];
  dataSource!: MatTableDataSource<any>;

  @ViewChild('paginator') paginator!: MatPaginator;
  @ViewChild(MatSort) matSort!: MatSort;

  constructor(
    private customersService: CustomersService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.customersService.getCustomersList().subscribe((response: any) => {
      this.dataSource = new MatTableDataSource(response);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.matSort;
    });
  }

  filterData($event: any) {
    this.dataSource.filter = $event.target.value;
  }

  refreshCustomers() {
    this.customersService.getCustomersList().subscribe((response: any) => {
      this.dataSource = new MatTableDataSource(response);
    });
  }

  async showCustomerDetails(customerId: number): Promise<void> {
    try {
      await this.router.navigate(['/customers/details', customerId]);
    } catch (error) {
      // Gérer l'erreur ici
      console.error(error);
    }
  }

  deleteCustomer(id: number, customerFirstname: string) {
    let conf = confirm(
      `Êtes-vous sûr de vouloir supprimer ${customerFirstname}`
    );
    if (conf) {
      this.customersService.deleteCustomer(id).subscribe(() => {
        this.refreshCustomers();
      });
    }
  }

  editCustomer(customerId: number) {
    this.router.navigate(['/customers/edit', customerId]);
  }
}
