package com.solennnaderludovic.m1.mangementquality.controller;


import com.solennnaderludovic.m1.mangementquality.model.Customer;
import com.solennnaderludovic.m1.mangementquality.service.CustomerService;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("customer")
@CrossOrigin("*")
@Tag(name = "Customer")
public class CustomerController {

    private final CustomerService service;

    public CustomerController(CustomerService service) {
        this.service = service;
    }

    @GetMapping
    @ApiResponse(responseCode = "200", description = "List of Customer returned")
    @ApiResponse(responseCode = "204", description = "Empty List")
    @ApiResponse(responseCode = "500", description = "Internal Server Error")
    public ResponseEntity<List<Customer>> findAll() {
        try {
            List<Customer> customers = service.findAll();
            if (customers.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("{id}")
    @ApiResponse(responseCode = "200", description = "Customer returned")
    @ApiResponse(responseCode = "404", description = "Customer Not Found")
    public ResponseEntity<Customer> findById(@PathVariable Long id) {
        try {
            Customer customer = service.findById(id);
            return new ResponseEntity<>(customer, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping()
    @ApiResponse(responseCode = "201", description = "Customer Created")
    @ApiResponse(responseCode = "500", description = "Internal Server Error")
    public ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) {
        try {
            Customer customerSaved = service.save(customer);
            return new ResponseEntity<>(customerSaved, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PatchMapping()
    @ApiResponse(responseCode = "20O", description = "Customer Modified")
    @ApiResponse(responseCode = "500", description = "Internal Server Error")
    public ResponseEntity<Customer> patch(@RequestBody Customer customer) {
        try {
            Customer customerSaved = service.save(customer);
            return new ResponseEntity<>(customerSaved, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("{id}")
    @ApiResponse(responseCode = "200", description = "Customer returned")
    @ApiResponse(responseCode = "404", description = "Customer Not Found")
    public ResponseEntity<HttpStatus> deleteById(@PathVariable Long id) {
        try {
            service.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
