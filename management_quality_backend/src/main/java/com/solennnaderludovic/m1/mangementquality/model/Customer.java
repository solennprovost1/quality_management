package com.solennnaderludovic.m1.mangementquality.model;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "customers")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String firstname;

    @Column
    private String lastname;

    @Column
    private String dateOfBirth;

    @Column
    private String address;

    @Column
    private String zipCode;

    @Column
    private String city;

}
