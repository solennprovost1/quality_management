package com.solennnaderludovic.m1.mangementquality.service;

import com.solennnaderludovic.m1.mangementquality.model.Customer;
import com.solennnaderludovic.m1.mangementquality.repository.CustomerRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    private final CustomerRepository repository;

    public CustomerService(CustomerRepository repository) {
        this.repository = repository;
    }

    public List<Customer> findAll() {
        return repository.findAll();
    }

    public Customer findById(Long id) {
        return repository.findById(id).orElseThrow();
    }

    public Customer save(Customer customer) {
        return repository.saveAndFlush(customer);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }


}
