package com.solennnaderludovic.m1.mangementquality.repository;

import com.solennnaderludovic.m1.mangementquality.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
