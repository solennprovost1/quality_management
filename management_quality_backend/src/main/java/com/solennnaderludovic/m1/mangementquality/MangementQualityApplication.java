package com.solennnaderludovic.m1.mangementquality;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource("classpath:.env")
public class MangementQualityApplication {

	public static void main(String[] args) {
		SpringApplication.run(MangementQualityApplication.class, args);
	}

}
