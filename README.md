# Introduction et auteurs du projet
Nader Gousmi, Solenn Provost et Ludovic Angenard ont crée une application web simplifiée pour utiliser des outils de qualité d'application.
# Contenu du repo
Le front est en Angular version 15
Le back est en Java version 17 avec spring boot 3.1.0

Le projet donctionne grâce à docker donc il y'a dans les deux projets un fichier dockerfile. Un fichier docker-compose.yml se trouve à la racine.

# Fonctionnement
Le docker-compose lance les services. Les services sont :
* back
* front
* bdd postgresql
* sonarQube
* jenkins

Les liaisons entre les différents services se présentent comme ceci :

![schéma d'architecture](/source_readme/archi.png)

# End-point
l'api répond à ces différents points d'entrées :
end point | code HTTP | description
 --- | --- | ---
customer | GET | retourne une liste de customer
customer/{id} | GET | retourne un customer
customer/{id} | DELETE | supprime un customer
customer | POST | ajoute un customer
customer | PUT | met à jour un customer

